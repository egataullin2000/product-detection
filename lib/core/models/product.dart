class Product {
  final String name;
  final double originalPrice;
  final int discount;
  final double discountedPrice;

  const Product(this.name, this.originalPrice,
      {this.discount, this.discountedPrice});

  bool get hasDiscount => discount != null;
}
