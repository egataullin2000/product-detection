import 'dart:ui';

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:ml_vision_app/core/models/product.dart';

abstract class ProductDetectionService {
  Future<Product> detectProductInfo(FirebaseVisionImage image, {Rect scanArea});
}
