import 'dart:ui';

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:ml_vision_app/core/models/product.dart';

import 'product_detection_service.dart';

class ProductDetectionServiceImpl implements ProductDetectionService {
  @override
  Future<Product> detectProductInfo(
    FirebaseVisionImage image, {
    Rect scanArea,
  }) =>
      Future(() async {
        final visionText =
            await FirebaseVision.instance.textRecognizer().processImage(image);
        var blocks = visionText.blocks;
        if (scanArea != null) {
          blocks = visionText.blocks
              .where((block) => scanArea.overlaps(block.boundingBox))
              .toList(growable: false);
        }
        if (blocks.length < 2) return null;

        Product product;

        final priceBlockIndex = _detectPriceBlockIndex(blocks);
        try {
          if (priceBlockIndex != null) {
            double originalPrice;
            int discount;
            double discountedPrice;
            final price1 = _detectPrice(blocks[priceBlockIndex]);
            var price2 = _detectPrice(blocks[priceBlockIndex - 1]);
            if (price2 == null) {
              originalPrice = price1;
            } else if (price2 > price1) {
              discount = _detectDiscount(blocks[priceBlockIndex - 2]);
              if (discount != null) {
                final discountError = discount * 0.5;
                // If has discount original price with pennies always goes in
                // one block: not 279.00 but 27900
                var isDiscountErrorSufficient = _isDiscountErrorSufficient(
                  discount,
                  _getRealDiscount(price2, price1),
                  discountError,
                );
                while (isDiscountErrorSufficient && price2 > 0) {
                  price2 /= 10;
                  isDiscountErrorSufficient = _isDiscountErrorSufficient(
                    discount,
                    _getRealDiscount(price2, price1),
                    discountError,
                  );
                }
                if (!isDiscountErrorSufficient) {
                  originalPrice = price2;
                  discountedPrice = price1;
                }
              }
            }
            if (originalPrice != null) {
              final name = discount == null
                  ? discountedPrice == null
                      ? _detectName(blocks, priceBlockIndex - 1)
                      : null
                  : discountedPrice == null
                      ? null
                      : _detectName(blocks, priceBlockIndex - 3);
              if (name != null) {
                product = Product(
                  name,
                  originalPrice,
                  discount: discount,
                  discountedPrice: discountedPrice,
                );
              }
            }
          }
        } on RangeError {}

        return product;
      });

  /// Detects TextBlock with price by searching the biggest TextBlock with
  /// numbers only
  ///
  /// The result will be a discounted price if there is a discount or the
  /// original price if not
  int _detectPriceBlockIndex(List<TextBlock> blocks) {
    int priceBlockIndex;
    for (var i = 0; i < blocks.length; i++) {
      final priceBlock =
          priceBlockIndex == null ? null : blocks[priceBlockIndex];
      final currentBlock = blocks[i];
      if (currentBlock.lines.length == 1) {
        final isBigger = currentBlock.boundingBox.height >
            (priceBlock?.boundingBox?.height ?? 0);
        if (isBigger) {
          final elements = currentBlock.lines[0].elements;
          if (elements.length == 2) {
            final asNumbers = elements.map((e) => int.tryParse(e.text));
            if (asNumbers.every((e) => e != null && e >= 0)) {
              priceBlockIndex = i;
            }
          }
        }
      }
    }

    return priceBlockIndex;
  }

  /// Detects price from TextBlock
  /// If accurate is true returns price only if pennies has been detected
  double _detectPrice(TextBlock block) {
    double price;

    final elements = block.lines[0].elements;
    if (elements.length <= 2) {
      final pricePieces = elements.map((e) => int.tryParse(e.text));
      if (pricePieces.every((element) => element != null)) {
        price = pricePieces.first.toDouble();
        if (pricePieces.length == 2) price += pricePieces.last / 100;
      }
    }

    return price;
  }

  /// Returns discount if it is in this TextBlock or null if not
  int _detectDiscount(TextBlock block) {
    final data = block.text;
    return _isDiscount(data)
        ? int.parse(data.substring(1, data.length - 1))
        : null;
  }

  bool _isDiscount(String data) => RegExp(r'\-[0-9]+\%').hasMatch(data);

  /// The product name is often divided into different TextBlocks
  ///
  /// This method collects them together, checks if it turned out to be a number
  /// and returns the name if not
  String _detectName(List<TextBlock> blocks, int lastNameBlockIndex) {
    final lastBlockText = blocks[lastNameBlockIndex].text;
    String name;
    if (!_isDiscount(lastBlockText)) {
      name = '';
      final endBlockBoundingBox = blocks[lastNameBlockIndex].boundingBox;
      blocks.sublist(0, lastNameBlockIndex).forEach((block) {
        final boundingBox = block.boundingBox;
        final xDistance = (endBlockBoundingBox.left - boundingBox.left).abs();
        final xMeasurementError = 0.1 * boundingBox.width;
        final yDistance = (endBlockBoundingBox.top - boundingBox.bottom).abs();
        final yMeasurementError = 0.5 * boundingBox.height;
        if (xDistance < xMeasurementError && yDistance < yMeasurementError) {
          name += block.text;
        }
      });
      name += lastBlockText;
      name = name.replaceAll('\n', ' ');
      name = double.tryParse(name) == null ? name : null;
    }

    return name;
  }

  double _getRealDiscount(double originalPrice, double discountedPrice) =>
      (originalPrice - discountedPrice) / originalPrice * 100;

  bool _isDiscountErrorSufficient(
          int expectedDiscount, double realDiscount, double error) =>
      (realDiscount - expectedDiscount).abs() > error;
}
