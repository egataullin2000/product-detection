import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

abstract class VisionImageBuildingService {
  FirebaseVisionImage build(CameraImage image, int rotation);
}
