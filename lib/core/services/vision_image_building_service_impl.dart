import 'dart:typed_data';
import 'dart:ui';

import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/foundation.dart';

import 'vision_image_building_service.dart';

class VisionImageBuildingServiceImpl implements VisionImageBuildingService {
  @override
  FirebaseVisionImage build(CameraImage image, int rotation) =>
      FirebaseVisionImage.fromBytes(
        _concatenatePlanes(image.planes),
        _buildMetaData(image, _imageRotationFrom(rotation)),
      );

  Uint8List _concatenatePlanes(List<Plane> planes) {
    final bytes = WriteBuffer();
    planes.forEach((plane) => bytes.putUint8List(plane.bytes));
    return bytes.done().buffer.asUint8List();
  }

  FirebaseVisionImageMetadata _buildMetaData(
    CameraImage image,
    ImageRotation rotation,
  ) =>
      FirebaseVisionImageMetadata(
        rawFormat: image.format.raw,
        size: Size(image.width.toDouble(), image.height.toDouble()),
        rotation: rotation,
        planeData: image.planes
            .map((plane) => FirebaseVisionImagePlaneMetadata(
                  bytesPerRow: plane.bytesPerRow,
                  height: plane.height,
                  width: plane.width,
                ))
            .toList(),
      );

  ImageRotation _imageRotationFrom(int rotation) {
    assert(rotation % 90 == 0);
    return rotation == 0
        ? ImageRotation.rotation0
        : rotation == 90
            ? ImageRotation.rotation90
            : rotation == 180
                ? ImageRotation.rotation180
                : ImageRotation.rotation270;
  }
}
