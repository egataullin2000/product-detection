import 'package:flutter/material.dart';
import 'package:injector/injector.dart';
import 'package:ml_vision_app/ui/product_detection/product_detection_model.dart';
import 'package:provider/provider.dart';

import 'core/services/product_detection_service.dart';
import 'core/services/product_detection_service_impl.dart';
import 'core/services/vision_image_building_service.dart';
import 'core/services/vision_image_building_service_impl.dart';
import 'ui/product_detection/product_detection_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyApp({Key key}) : super(key: key) {
    final registerSingleton = Injector.appInstance.registerSingleton;
    registerSingleton<ProductDetectionService>(
      (_) => ProductDetectionServiceImpl(),
    );
    registerSingleton<VisionImageBuildingService>(
      (_) => VisionImageBuildingServiceImpl(),
    );
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'MlVision',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          textTheme: Theme.of(context).textTheme.apply(bodyColor: Colors.white),
        ),
        home: ChangeNotifierProvider(
          create: (_) => ProductDetectionModel(),
          child: ProductDetectionPage(),
        ),
      );
}
