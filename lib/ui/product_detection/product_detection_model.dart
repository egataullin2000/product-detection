import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/widgets.dart';
import 'package:injector/injector.dart';
import 'package:ml_vision_app/core/models/product.dart';
import 'package:ml_vision_app/core/services/product_detection_service.dart';

class ProductDetectionModel extends ChangeNotifier {
  final _cameraPreviewKey = GlobalKey();
  final _scanAreaKey = GlobalKey();
  final _productDetectionService =
      Injector.appInstance.getDependency<ProductDetectionService>();
  Product _product;
  double _imageHeight;

  Key get cameraPreviewKey => _cameraPreviewKey;

  Key get scanAreaKey => _scanAreaKey;

  Product get product => _product;

  // image.height returns width, so height = image.width
  void onNewImage(CameraImage image) => _imageHeight = image.width.toDouble();

  Future<Product> detect(FirebaseVisionImage image) async {
    final product = _productDetectionService.detectProductInfo(
      image,
      scanArea: _getScanArea(),
    );

    return product;
  }

  Rect _getScanArea() {
    final RenderBox scanAreaRenderBox =
        _scanAreaKey.currentContext.findRenderObject();
    final onScreenSize = scanAreaRenderBox.size;
    final onScreenPosition = scanAreaRenderBox.localToGlobal(Offset.zero);
    final ratio = _imageHeight / _cameraPreviewKey.currentContext.size.height;

    return onScreenPosition * ratio & onScreenSize * ratio;
  }

  void onProductDetected(Product product) {
    if (product != null) {
      _product = product;
      notifyListeners();
    }
  }
}
