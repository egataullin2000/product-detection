import 'package:flutter/material.dart';
import 'package:ml_vision_app/core/models/product.dart';
import 'package:ml_vision_app/ui/product_detection/product_detection_model.dart';
import 'package:ml_vision_app/ui/widgets/camera_ml_vision.dart';
import 'package:provider/provider.dart';

class ProductDetectionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<ProductDetectionModel>(context, listen: false);
    return Scaffold(
      body: Stack(children: [
        CameraMlVision<Product>(
          key: model.cameraPreviewKey,
          detect: model.detect,
          onNewImage: model.onNewImage,
          onResult: model.onProductDetected,
          overlayBuilder: (context) => Padding(
            padding: const EdgeInsets.all(24.0),
            child: AspectRatio(
              key: model.scanAreaKey,
              aspectRatio: 1 / 1,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                    width: 4,
                  ),
                ),
              ),
            ),
          ),
        ),
        Consumer<ProductDetectionModel>(
          builder: (_, model, __) => model.product == null
              ? const SizedBox()
              : _buildProductInfo(model.product),
        ),
      ]),
    );
  }

  Widget _buildProductInfo(Product product) {
    final children = [
      Text('Product name: ${product.name}'),
      Text('Original price: ${product.originalPrice.toString()} руб'),
    ];
    if (product.hasDiscount) {
      children.addAll([
        Text('Discount: ${product.discount}%'),
        Text('Discounted price: ${product.discountedPrice} руб'),
      ]);
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: children,
    );
  }
}
