import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:injector/injector.dart';
import 'package:ml_vision_app/core/services/vision_image_building_service.dart';

class CameraMlVision<T> extends StatefulWidget {
  final Future<T> Function(FirebaseVisionImage) detect;
  final Function(T) onResult;
  final WidgetBuilder placeholderBuilder, overlayBuilder;
  final Function(CameraImage) onNewImage;
  final ResolutionPreset resolution;

  const CameraMlVision({
    Key key,
    @required this.detect,
    @required this.onResult,
    this.placeholderBuilder,
    this.overlayBuilder,
    this.onNewImage,
    this.resolution = ResolutionPreset.high,
  })  : assert(detect != null),
        assert(onResult != null),
        super(key: key);

  @override
  _CameraMlVisionState createState() => _CameraMlVisionState<T>();
}

class _CameraMlVisionState<T> extends State<CameraMlVision<T>>
    with WidgetsBindingObserver {
  CameraController _cameraController;
  var _isProcessing = false;
  final _visionImageBuildingService =
      Injector.appInstance.getDependency<VisionImageBuildingService>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _initCameraController();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (state == AppLifecycleState.inactive) {
      _cameraController?.dispose();
      _cameraController = null;
    } else if (state == AppLifecycleState.resumed) {
      _initCameraController();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget body;
    if (_cameraController == null || !_cameraController.value.isInitialized) {
      body = widget.placeholderBuilder?.call(context) ?? Container();
    } else {
      body = CameraPreview(_cameraController);
      if (widget.overlayBuilder != null) {
        body = Stack(
          alignment: Alignment.center,
          children: [
            body,
            widget.overlayBuilder(context),
          ],
        );
      }
    }

    return body;
  }

  void _initCameraController() async {
    _cameraController = CameraController(
      (await availableCameras())[0],
      ResolutionPreset.high,
      enableAudio: false,
    );
    _cameraController.initialize().then((_) {
      if (mounted) {
        setState(() {});
        _cameraController.startImageStream(_handleImage);
      }
    });
  }

  void _handleImage(CameraImage image) async {
    if (!_isProcessing && mounted) {
      _isProcessing = true;
      widget.onNewImage?.call(image);
      widget.onResult(await _processImage(image));
      _isProcessing = false;
    }
  }

  Future<T> _processImage(CameraImage image) =>
      widget.detect(_visionImageBuildingService.build(
        image,
        _cameraController.description.sensorOrientation,
      ));
}
